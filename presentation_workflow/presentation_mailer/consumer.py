import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

def process_message():
    send_mail()


parameters = pika.ConnectionParameters(host='rabbitmq')
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue='presentation_approvals')
channel.queue_declare(queue='presentation_rejections')
channel.basic_consume(
    queue='presentation_approvals',
    on_message_callback=process_message,
    auto_ack=True,
)
channel.start_consuming()