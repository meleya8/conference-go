from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json

# def PEXELS_KEY():
#     url = "https://api.pexels.com/v1/search?query=people"
#     header = {"Authorization": PEXELS_API_KEY}
#     response = request.get(url, headers=header)
#     print(response.body)


def find_pexels(term):
    url = "https://api.pexels.com/v1/search?query=" + term

    header = {"Authorization": PEXELS_API_KEY}

    response = requests.get(url, headers=header)

    data = json.loads(response.content)

    photos = data["photos"]

    return photos


def get_weather_data(city, state):
    url = (
        "http://api.openweathermap.org/geo/1.0/direct?q= + city"
        + ","
        + state
        + "&limit=5{limit}&appid={API key}"
        + OPEN_WEATHER_API_KEY
    )
